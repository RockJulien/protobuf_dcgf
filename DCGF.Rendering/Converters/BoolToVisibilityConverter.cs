﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace DCGF.Rendering.Converters
{
    /// <summary>
    /// Definition of the <see cref="BoolToVisibilityConverter"/> class.
    /// </summary>
    public class BoolToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// Converts
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.GetType() != typeof(bool))
            {
                throw new InvalidOperationException("The value must be a bool");
            }

            bool lValue = (bool)value;
            return lValue ? Visibility.Visible : Visibility.Hidden;
        }

        /// <summary>
        /// Converts back
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
