﻿using DCGF.Rendering.DataModel;
using DCGF.Rendering.ViewModel;
using DirectCGF.Common;
using DirectCGF.Constants;
using Google.Protobuf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace DCGF.Rendering
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Dependencies

        /// <summary>
        /// Entities property
        /// </summary>
        public static readonly DependencyProperty EntitiesProperty = DependencyProperty.Register( "Entities", typeof(ObservableCollection<EntityViewModel>), typeof(MainWindow), new PropertyMetadata( null ) );

        #endregion Dependencies

        #region Fields

        /// <summary>
        /// Stores the set of view models.
        /// </summary>
        private ObservableCollection<EntityViewModel> mViewModels;

        /// <summary>
        /// Stores the current entity to select.
        /// </summary>
        private AEntity mCurrentEntity = null;

        /// <summary>
        /// Stores the set of visual entities.
        /// </summary>
        private Dictionary<string, AEntity> mEntities;

        /// <summary>
        /// Stores the initial specific topic to care about.
        /// </summary>
        private HashSet<string> mTopic;

        /// <summary>
        /// Stores the client to get entity server update.
        /// </summary>
        private Client mClient;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the set of view models.
        /// </summary>
        public ObservableCollection<EntityViewModel> Entities
        {
            get
            {
                return (ObservableCollection<EntityViewModel>)GetValue(EntitiesProperty);
            }
            set
            {
                SetValue( EntitiesProperty, value );
            }
        }

        /// <summary>
        /// Gets the window background image name.
        /// </summary>
        public string BGImage
        {
            get
            {
                return "6.jpg";
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.DataContext = this;
            this.mTopic = new HashSet<string>();
            this.mEntities = new Dictionary<string, AEntity>();
            this.mViewModels = new ObservableCollection<EntityViewModel>();
            this.Entities = this.mViewModels;

            this.InitializeComponent();

            try
            {
                this.mClient = new Client( new Context(), Constants.SIMULATIONCLIENTENDPOINT );
                this.mClient.MessageReceived += this.OnMessageReceived;
                this.mClient.Start();
                this.mClient.SubscribeToTopic( Constants.SIMULATIONTOPIC );
            }
            catch
            {
                Console.WriteLine( "Error when creating the client..." );
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on message received.
        /// </summary>
        /// <param name="pTopic">The topic if any.</param>
        /// <param name="pMessage">The message.</param>
        private void OnMessageReceived(string pTopic, IMessage pMessage)
        {
            if ( System.Windows.Application.Current == null )
            {
                return;
            }

            bool lIsNew      = false;
            bool lIsCurrent  = false;
            bool lMustRemove = false;
            // Dispatch to the main thread!!!
            System.Windows.Application.Current?.Dispatcher.BeginInvoke( System.Windows.Threading.DispatcherPriority.Background, (Action)delegate 
            {
                AEntity lEntity  = SimulationHelpers.CreateFromMessage( typeof(VisualEntity), ref this.mEntities, pMessage, out lIsNew, out lMustRemove, out lIsCurrent );
                if ( lEntity != null )
                {
                    if ( lIsCurrent )
                    {
                        // Update current.
                        this.mCurrentEntity = lEntity;
                    }
                
                    EntityViewModel lVM = null;
                    if ( lIsNew )
                    {
                        // Add it.
                        lVM = new EntityViewModel( lEntity as VisualEntity );
                        this.mViewModels.Add( lVM );
                    }
                    else
                    {
                        lVM = this.mViewModels.FirstOrDefault( pElt => pElt.OwnedObject == lEntity );
                    }
                
                    if ( lVM != null )
                    {
                        if ( lMustRemove )
                        {
                            // Remove it.
                            lVM.Dispose();
                            this.mViewModels.Remove( lVM );
                        }
                        else 
                        {
                            lVM.IsCurrent = lIsCurrent;
                        }
                    }

                    InvalidateProperty( EntitiesProperty );
                
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnEarthMapLoaded(object sender, RoutedEventArgs e)
        {
            // ... Create a new BitmapImage.
            BitmapImage b = new BitmapImage();
            b.BeginInit();
            b.UriSource = new Uri( Environment.CurrentDirectory + "/../Resources/6.jpg" );
            b.EndInit();

            // ... Get Image reference from sender.
            var image = sender as Image;
            // ... Assign Source.
            image.Source = b;
        }

        #endregion Methods
    }
}
