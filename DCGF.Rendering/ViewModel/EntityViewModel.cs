﻿using DCGF.Rendering.DataModel;
using DirectCGF.Constants;
using System;
using System.ComponentModel;
using System.Windows.Media;

namespace DCGF.Rendering.ViewModel
{
    /// <summary>
    /// Definition of the <see cref="EntityViewModel"/> class.
    /// </summary>
    public class EntityViewModel : INotifyPropertyChanged, IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the entity image name.
        /// </summary>
        private string mEntityImage;

        /// <summary>
        /// Stores the flag indicating whether the entity is the current one or not.
        /// </summary>
        private bool mIsCurrent;

        /// <summary>
        /// Stores the X position.
        /// </summary>
        private double mX;

        /// <summary>
        /// Stores the Y position.
        /// </summary>
        private double mY;

        /// <summary>
        /// Stores the entity heading.
        /// </summary>
        private double mHeading;

        /// <summary>
        /// Stores the entity color.
        /// </summary>
        private Color mColor;

        /// <summary>
        /// Stores the owned entity.
        /// </summary>
        private VisualEntity mOwnedObject;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the entity image name.
        /// </summary>
        public string EntityImage
        {
            get
            {
                return this.mEntityImage;
            }
            set
            {
                this.mEntityImage = value;
                this.NotifyPropertyChanged( "EntityImage" );
            }
        }

        /// <summary>
        /// Gets the owned object.
        /// </summary>
        public object OwnedObject
        {
            get
            {
                return this.mOwnedObject;
            }
        }

        /// <summary>
        /// Gets the display string.
        /// </summary>
        public string DisplayString
        {
            get
            {
                return this.mOwnedObject.Name;
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the entity is the current one or not.
        /// </summary>
        public bool IsCurrent
        {
            get
            {
                return this.mIsCurrent;
            }
            set
            {
                this.mIsCurrent = value;
                this.NotifyPropertyChanged( "IsCurrent" );
            }
        }

        /// <summary>
        /// Gets or sets the X position.
        /// </summary>
        public double X
        {
            get
            {
                return this.mX;
            }
            set
            {
                this.mX = value;
                this.NotifyPropertyChanged( "X" );
            }
        }

        /// <summary>
        /// Gets or sets the Y position.
        /// </summary>
        public double Y
        {
            get
            {
                return this.mY;
            }
            set
            {
                this.mY = value;
                this.NotifyPropertyChanged( "Y" );
            }
        }

        /// <summary>
        /// Gets or sets the entity heading in degrees.
        /// </summary>
        public double Heading
        {
            get
            {
                return this.mHeading;
            }
            set
            {
                this.mHeading = value;
                this.NotifyPropertyChanged( "Heading" );
            }
        }

        /// <summary>
        /// Gets or sets the entity color.
        /// </summary>
        public Color Color
        {
            get
            {
                return this.mColor;
            }
            set
            {
                this.mColor = value;
                this.NotifyPropertyChanged( "Color" );
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityViewModel"/> class.
        /// </summary>
        /// <param name="pDataModel">The visual entity</param>
        public EntityViewModel(VisualEntity pDataModel)
        {
            this.mOwnedObject = pDataModel;
            this.mX = pDataModel.Position.Longitude;
            this.mY = pDataModel.Position.Latitude;
            this.mHeading = pDataModel.Orientation.Yaw;
            this.mColor = Colors.White;
            this.EntityImage = "White.png";
            if (pDataModel.Team.ColorId == Constants.BLUETEAMCOLORID)
            {
                this.mColor = Colors.Blue;
                this.EntityImage = "Blue.png";
            }
            else if (pDataModel.Team.ColorId == Constants.REDTEAMCOLORID)
            {
                this.mColor = Colors.Red;
                this.EntityImage = "Red.png";
            }
            else if (pDataModel.Team.ColorId == Constants.GREENTEAMCOLORID)
            {
                this.mColor = Colors.Green;
                this.EntityImage = "Green.png";
            }
            this.mIsCurrent   = true;
            this.mOwnedObject.PropertyChanged += this.OnOwnedObjectPropertyChanged;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on owned object property changes
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnOwnedObjectPropertyChanged(object pSender, PropertyChangedEventArgs pEventArgs)
        {
            // Propagates
            if ( pEventArgs.PropertyName == "Position" )
            {
                this.Y = this.mOwnedObject.Position.Latitude;
                this.X = this.mOwnedObject.Position.Longitude;
            }
            else if ( pEventArgs.PropertyName == "Orientation" )
            {
                this.Heading = this.mOwnedObject.Orientation.Yaw;
            }
            else if ( pEventArgs.PropertyName == "State" )
            {
                if ( this.mOwnedObject.State == DirectCGF.Common.EntityState.Alive )
                {
                    this.EntityImage = "White.png";
                    if ( this.mOwnedObject.Team.ColorId == Constants.BLUETEAMCOLORID )
                    {
                        this.EntityImage = "Blue.png";
                    }
                    else if ( this.mOwnedObject.Team.ColorId == Constants.REDTEAMCOLORID )
                    {
                        this.EntityImage = "Red.png";
                    }
                    else if ( this.mOwnedObject.Team.ColorId == Constants.GREENTEAMCOLORID )
                    {
                        this.EntityImage = "Green.png";
                    }
                }
                else if ( this.mOwnedObject.State == DirectCGF.Common.EntityState.Dead )
                {
                    this.EntityImage = "WhiteDead.png";
                    if ( this.mOwnedObject.Team.ColorId == Constants.BLUETEAMCOLORID )
                    {
                        this.EntityImage = "BlueDead.png";
                    }
                    else if ( this.mOwnedObject.Team.ColorId == Constants.REDTEAMCOLORID )
                    {
                        this.EntityImage = "RedDead.png";
                    }
                    else if ( this.mOwnedObject.Team.ColorId == Constants.GREENTEAMCOLORID )
                    {
                        this.EntityImage = "GreenDead.png";
                    }
                }
            }
            else if ( pEventArgs.PropertyName == "Team" )
            {
                Color lNewColor =  Colors.White;
                this.EntityImage = "White.png";
                if ( this.mOwnedObject.Team.ColorId == Constants.BLUETEAMCOLORID )
                {
                    lNewColor = Colors.Blue;
                    this.EntityImage = "Blue.png";
                }
                else if ( this.mOwnedObject.Team.ColorId == Constants.REDTEAMCOLORID )
                {
                    lNewColor = Colors.Red;
                    this.EntityImage = "Red.png";
                }
                else if ( this.mOwnedObject.Team.ColorId == Constants.GREENTEAMCOLORID )
                {
                    lNewColor = Colors.Green;
                    this.EntityImage = "Green.png";
                }

                this.Color = lNewColor;
            }
        }

        /// <summary>
        /// Notifies a property changed.
        /// </summary>
        /// <param name="pPropertyName"></param>
        private void NotifyPropertyChanged(string pPropertyName)
        {
            if ( this.PropertyChanged != null )
            {
                this.PropertyChanged( this, new PropertyChangedEventArgs( pPropertyName ) );
            }
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mOwnedObject != null )
            {
                this.mOwnedObject = null;
            }
        }

        #endregion Methods
    }
}
