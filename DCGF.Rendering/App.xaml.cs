﻿using System.Diagnostics;
using System.Windows;

namespace DCGF.Rendering
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Stores the server process name.
        /// </summary>
        private string mServerProcessName;

        /// <summary>
        /// Stores the server process to synch the close.
        /// </summary>
        private Process mServerProcess = null;

        /// <summary>
        /// Initializes a new instance of <see cref="App"/> class.
        /// </summary>
        public App()
        {
            // Launch the server.
            LaunchCommandLineApp();
        }

        /// <summary>
        /// Launch the legacy application with some options set.
        /// </summary>
        void LaunchCommandLineApp()
        {
            //// If any args
            //const string Arg1 = "C:\\";
            //const string Arg2 = "C:\\Dir";

            // Use ProcessStartInfo class
            ProcessStartInfo lStartInfo = new ProcessStartInfo();
            lStartInfo.CreateNoWindow  = false;
            lStartInfo.UseShellExecute = false;
            lStartInfo.FileName = "DCGF.Simulation.exe";
            lStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            //lStartInfo.Arguments = "-f j -o \"" + Arg1 + "\" -z 1.0 -s y " + Arg2;

            try
            {
                this.mServerProcess = Process.Start(lStartInfo);
                this.mServerProcessName = this.mServerProcess.ProcessName;
            }
            catch
            {
                // Log error.
                this.mServerProcess = null;
            }
        }
        
        /// <summary>
        /// Delegate called on application exit.
        /// </summary>
        /// <param name="pArgs"></param>
        protected override void OnExit(ExitEventArgs pArgs)
        {
            if ( this.mServerProcess != null )
            {
                Process[] lResult = Process.GetProcessesByName( this.mServerProcessName );
                if ( lResult != null && lResult.Length > 0 )
                {
                    this.mServerProcess.Kill();
                    this.mServerProcess = null;
                }
            }

            base.OnExit( pArgs );
        }
    }
}
