﻿using DirectCGF.Common;

namespace DCGF.Rendering.DataModel
{
    /// <summary>
    /// Definition of the visual <see cref="VisualEntity"/> class.
    /// </summary>
    public class VisualEntity : AEntity
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualEntity"/> class.
        /// </summary>
        /// <param name="pName">The entity name.</param>
        /// <param name="pTeam">The entity team.</param>
        public VisualEntity(string pName, Team pTeam) :
        base( pName, pTeam )
        {
            
        }

        #endregion Constructor
    }
}
