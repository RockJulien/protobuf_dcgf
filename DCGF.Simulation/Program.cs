﻿using DCGF.Simulation.DataModel;
using DirectCGF.Common;
using DirectCGF.Constants;
using Google.Protobuf;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DCGF.Simulation
{
    /// <summary>
    /// Simulation data server program
    /// 
    /// NOTE: Behavior like a mini simulation, allowing to create, remove, modify entity state.
    /// </summary>
    class Program
    {
        private static Server sSimulation;

        /// <summary>
        /// Stores the entity counter.
        /// </summary>
        private static int mCounter = 0;

        /// <summary>
        /// Stores the set of simulation entities
        /// </summary>
        private static HashSet<AEntity> sEntities;

        /// <summary>
        /// Stores the current entity.
        /// </summary>
        private static AEntity sCurrentEntity;

        /// <summary>
        /// Entry point.
        /// </summary>
        /// <param name="pArgs"></param>
        static void Main(string[] pArgs)
        {
            // Start the server.
            bool lStop = false;
            sEntities = new HashSet<AEntity>();
            sSimulation = new Server( new Context(), Constants.SIMULATIONCLIENTENDPOINT );

            Console.WriteLine( "Starting simulation..." );

            sSimulation.Start();
            
            while ( lStop == false )
            {
                bool lAnyUpdate = false;
                if ( Console.KeyAvailable )
                {
                    Console.WriteLine( "Processing user console key..." );

                    ConsoleKeyInfo lInfo = Console.ReadKey( true );
                    if ( lInfo.Key == ConsoleKey.UpArrow )
                    {
                        MoveUp();
                        lAnyUpdate = true;
                    }
                    else if ( lInfo.Key == ConsoleKey.DownArrow )
                    {
                        MoveDown();
                        lAnyUpdate = true;
                    }
                    else if ( lInfo.Key == ConsoleKey.LeftArrow )
                    {
                        MoveLeft();
                        lAnyUpdate = true;
                    }
                    else if ( lInfo.Key == ConsoleKey.RightArrow )
                    {
                        MoveRight();
                    }
                    else if ( lInfo.Key == ConsoleKey.K )
                    {
                        KillEntity(); // Its own message published
                    }
                    else if ( lInfo.Key == ConsoleKey.L )
                    {
                        RevivalEntity();
                        lAnyUpdate = true;
                    }
                    else if ( lInfo.Key == ConsoleKey.N )
                    {
                        CreateEntity();
                        lAnyUpdate = true;
                    }
                    else if ( lInfo.Key == ConsoleKey.R )
                    {
                        RemoveEntity();
                        lAnyUpdate = true;
                    }
                    else if ( lInfo.Key == ConsoleKey.Escape )
                    {
                        lStop = true;
                    }
                }

                if ( lAnyUpdate )
                {
                    // Send an update for each entity
                    foreach ( var lEntity in sEntities )
                    {
                        IMessage lMessage = SimulationHelpers.CreateFromEntity( lEntity, lEntity == sCurrentEntity );
                        if ( lMessage != null )
                        {
                            sSimulation.Publish( Constants.SIMULATIONTOPIC, lMessage );
                        }
                    }
                }
            }

            Console.WriteLine( "Ending simulation..." );

            sSimulation.Stop();
            sSimulation.Dispose();
        }

        /// <summary>
        /// Moves the current entity Upward
        /// </summary>
        private static void MoveUp()
        {
            if ( sCurrentEntity != null )
            {
                Location lPosition = sCurrentEntity.Position;
                lPosition.Latitude += 10.0;

                Direction lOrientation = sCurrentEntity.Orientation;
                lOrientation.Yaw = 0.0; // Up in 2D

                Console.WriteLine( "Up..." );
            }
        }

        /// <summary>
        /// Moves the current entity Downward
        /// </summary>
        private static void MoveDown()
        {
            if ( sCurrentEntity != null )
            {
                Location lPosition = sCurrentEntity.Position;
                lPosition.Latitude -= 10.0;

                Direction lOrientation = sCurrentEntity.Orientation;
                lOrientation.Yaw = 180.0; // Down in 2D

                Console.WriteLine( "Down..." );
            }
        }

        /// <summary>
        /// Moves the current entity Leftward
        /// </summary>
        private static void MoveLeft()
        {
            if ( sCurrentEntity != null )
            {
                Location lPosition = sCurrentEntity.Position;
                lPosition.Longitude += 10.0;

                Direction lOrientation = sCurrentEntity.Orientation;
                lOrientation.Yaw = 270.0; // Left in 2D

                Console.WriteLine( "Left..." );
            }
        }

        /// <summary>
        /// Moves the current entity Rightward
        /// </summary>
        private static void MoveRight()
        {
            if ( sCurrentEntity != null )
            {
                Location lPosition = sCurrentEntity.Position;
                lPosition.Longitude -= 10.0;

                Direction lOrientation = sCurrentEntity.Orientation;
                lOrientation.Yaw = 90.0; // Right in 2D

                Console.WriteLine( "Right..." );
            }
        }

        /// <summary>
        /// Create a new entity
        /// </summary>
        private static void CreateEntity()
        {
            Console.WriteLine( "Please enter the team name..." );
            string lTeamName = Console.ReadLine();

            Console.WriteLine( "Please enter the team color id [0-3]..." );
            string lColorStr = Console.ReadLine();
            int lColorId = int.Parse( lColorStr );
            if ( lColorId < 0 ||
                 lColorId > 3 )
            {
                // Swap to default color due to bad choice.
                lColorId = 0;
                Console.WriteLine( "Wrong color id chosen, swapping to default team color..." );
            }

            // New entity
            Team lTeam = new Team( lTeamName, lColorId );
            SimEntity lNewEntity = new SimEntity( string.Format( "Rafale_{0}", mCounter++ ), lTeam );
            sEntities.Add( lNewEntity );

            // Current at creation by default.
            sCurrentEntity = lNewEntity;

            Console.WriteLine( "Entity created..." );
        }

        /// <summary>
        /// Kills the entity
        /// </summary>
        private static void KillEntity()
        {
            if ( sCurrentEntity != null )
            {
                // Auto swap to the last one as current.
                sCurrentEntity.State = EntityState.Dead;

                Console.WriteLine( "Entity killed..." );
            }
        }

        /// <summary>
        /// Revivals the entity
        /// </summary>
        private static void RevivalEntity()
        {
            if ( sCurrentEntity != null )
            {
                // Auto swap to the last one as current.
                sCurrentEntity.State = EntityState.Alive;

                Console.WriteLine( "Entity back to life..." );
            }
        }

        /// <summary>
        /// Removes the current entity
        /// </summary>
        private static void RemoveEntity()
        {
            if ( sCurrentEntity != null )
            {
                sEntities.Remove( sCurrentEntity );

                IMessage lTexto = SimulationHelpers.RemoveEntity( sCurrentEntity.Name );
                sSimulation.Publish( Constants.SIMULATIONTOPIC, lTexto );

                // Auto swap to the last one as current.
                sCurrentEntity = sEntities.LastOrDefault();

                Console.WriteLine( "Entity removed..." );
            }
        }
    }
}
