﻿using DirectCGF.Common;

namespace DCGF.Simulation.DataModel
{
    /// <summary>
    /// Definition of the simulation <see cref="SimEntity"/> class.
    /// </summary>
    public class SimEntity : AEntity
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SimEntity"/> class.
        /// </summary>
        /// <param name="pName">The entity name.</param>
        /// <param name="pTeam">The entity team.</param>
        public SimEntity(string pName, Team pTeam) :
        base( pName, pTeam )
        {
            
        }

        #endregion Constructor
    }
}
