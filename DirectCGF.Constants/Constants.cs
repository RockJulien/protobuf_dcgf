﻿namespace DirectCGF.Constants
{
    /// <summary>
    /// Definition of the constants
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Stores the major version of OrcNet
        /// </summary>
        public const string MAJOR = "1";

        /// <summary>
        /// Stores the major version of OrcNet
        /// </summary>
        public const string MINOR = ".0";

        /// <summary>
        /// Stores the build version of OrcNet
        /// </summary>
        public const string BUILD = ".0";

        /// <summary>
        /// Stores the revision version of OrcNet
        /// </summary>
        public const string REVISION = ".0";

        /// <summary>
        /// Stores the copyright of the product
        /// </summary>
        public const string COPYRIGHT = "ProtoBuf_CGF © 2017";

        /// <summary>
        /// Stores the company name of Diginext creators.
        /// </summary>
        public const string COMPANY = "Diginext";

        /// <summary>
        /// Stores the white (default) team color id.
        /// </summary>
        public const int WHITETEAMCOLORID = 0;

        /// <summary>
        /// Stores the blue team color id.
        /// </summary>
        public const int BLUETEAMCOLORID = 1;

        /// <summary>
        /// Stores the red team color id.
        /// </summary>
        public const int REDTEAMCOLORID = 2;

        /// <summary>
        /// Stores the green team color id.
        /// </summary>
        public const int GREENTEAMCOLORID = 3;

        /// <summary>
        /// Stores the white (default) team color id.
        /// </summary>
        public const string SIMULATIONTOPIC = "Simulation";

        /// <summary>
        /// Stores the constant simulation server endpoint.
        /// </summary>
        public const string SIMULATIONSERVERENDPOINT = "tcp://0.0.0.0:50010";

        /// <summary>
        /// Stores the constant simulation client endpoint.
        /// </summary>
        public const string SIMULATIONCLIENTENDPOINT = "tcp://127.0.0.1:50010";
    }
}
