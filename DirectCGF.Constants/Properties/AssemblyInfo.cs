﻿using DirectCGF.Constants;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DirectCGF.Constants")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(Constants.COMPANY)]
[assembly: AssemblyProduct("DirectCGF.Constants")]
[assembly: AssemblyCopyright(Constants.COPYRIGHT)]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("79d6a60b-26a0-435e-b10a-3eb0e7f7fbc6")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion(Constants.MAJOR + Constants.MINOR + Constants.BUILD + Constants.REVISION)]
[assembly: AssemblyFileVersion(Constants.MAJOR + Constants.MINOR + Constants.BUILD + Constants.REVISION)]
