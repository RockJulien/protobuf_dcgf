﻿using Castle.Zmq.Extensions;
using Google.Protobuf;
using System;
using System.Reflection;

namespace DirectCGF.Common
{
    /// <summary>
    /// Definition of the <see cref="Client"/> class.
    /// 
    /// NOTE: This client will be the view and will receive entity update to propagate it into
    /// the views.
    /// </summary>
    public class Client : BaseSubscriber<IMessage>
    {
        #region Fields
        
        /// <summary>
        /// Stores the context.
        /// </summary>
        private Context mContext = null;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on message received.
        /// </summary>
        public event Action<string, IMessage> MessageReceived;

        #endregion Events

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Client"/> class.
        /// </summary>
        /// <param name="pContext">The context</param>
        /// <param name="pEndpoint">The port message will come from.</param>
        public Client(Context pContext, string pEndpoint) :
        base( (Castle.Zmq.Context)pContext, pEndpoint, Deserialize )
        {
            this.mContext = pContext;            
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Releases resources.
        /// </summary>
        public new void Dispose()
        {
            if ( this.mContext != null )
            {
                this.mContext.Dispose();
                this.mContext = null;
            }

            base.Dispose();
        }
        
        /// <summary>
        /// Turns the given message into a byte array.
        /// </summary>
        /// <param name="pMessage">The PB message.</param>
        /// <returns>The byte array encoded in protobuf encoding.</returns>
        private static IMessage Deserialize(byte[] pMessage)
        {
            DCGF.ProtoBuf.GenericType lType = new DCGF.ProtoBuf.GenericType();
            lType.MergeFrom( pMessage );

            IMessage lMessage = null;
            string lTypeName = lType.TypeName.TrimStart( '[' );
            lTypeName = lTypeName.TrimEnd( ']' );
            Type lMessageType = Type.GetType( lTypeName );
            if ( lMessageType != null )
            {
                ConstructorInfo lCtor = lMessageType.GetConstructor( new Type[] { } );
                lMessage = lCtor.Invoke( new object[] { } ) as IMessage;
            }

            try
            {
                lMessage.MergeFrom( pMessage );
            }
            catch
            {
                return null;
            }

            return lMessage;   
        }

        /// <summary>
        /// Delegate called on message received.
        /// </summary>
        /// <param name="pTopic">The topic for interest purpose</param>
        /// <param name="pMessage">The message.</param>
        protected override void OnReceived(string pTopic, IMessage pMessage)
        {
            if ( pMessage == null )
            {
                return;
            }

            // Propagate to listeners.
            if ( this.MessageReceived != null )
            {
                this.MessageReceived( pTopic, pMessage );
            }
        }

        #endregion Methods
    }
}
