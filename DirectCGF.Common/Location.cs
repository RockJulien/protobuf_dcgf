﻿namespace DirectCGF.Common
{
    /// <summary>
    /// Definition of the <see cref="Location"/> class.
    /// </summary>
    public class Location
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Latitude component.
        /// </summary>
        public double Latitude
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Longitude component.
        /// </summary>
        public double Longitude
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Altitude component.
        /// </summary>
        public double Altitude
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Location"/> class.
        /// </summary>
        public Location() :
        this( 0.0, 0.0, 300.0 )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Location"/> class.
        /// </summary>
        /// <param name="pLatitude"></param>
        /// <param name="pLongitude"></param>
        /// <param name="pAltitude"></param>
        public Location(double pLatitude, double pLongitude, double pAltitude)
        {
            this.Latitude  = pLatitude;
            this.Longitude = pLongitude;
            this.Altitude  = pAltitude;
        }

        #endregion Constructor
    }
}
