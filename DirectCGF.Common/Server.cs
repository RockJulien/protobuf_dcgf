﻿using Castle.Zmq.Extensions;
using Google.Protobuf;

namespace DirectCGF.Common
{
    /// <summary>
    /// Definition of the <see cref="Server"/> class.
    /// 
    /// NOTE: This server will represent the simulation side and will publish entity updates
    /// but is made in such a way it is usable another ways.
    /// </summary>
    public class Server : BasePublisher<IMessage>
    {
        #region Fields

        /// <summary>
        /// Stores the context.
        /// </summary>
        private Context mContext = null;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Server"/> class.
        /// </summary>
        public Server(Context pContext, string pEndpoint) :
        base( (Castle.Zmq.Context)pContext, pEndpoint, Serialize )
        {
            this.mContext = pContext;
        }

        #endregion Constructor

        #region Methods
        
        /// <summary>
        /// Releases resources.
        /// </summary>
        public new void Dispose()
        {
            if ( this.mContext != null )
            {
                this.mContext.Dispose();
                this.mContext = null;
            }

            base.Dispose();
        }

        /// <summary>
        /// Turns the given message into a byte array.
        /// </summary>
        /// <param name="pMessage">The PB message.</param>
        /// <returns>The byte array encoded in protobuf encoding.</returns>
        private static byte[] Serialize(IMessage pMessage)
        {
            return pMessage.ToByteArray();
        }

        #endregion Methods
    }
}
