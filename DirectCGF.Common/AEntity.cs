﻿using System.ComponentModel;

namespace DirectCGF.Common
{
    /// <summary>
    /// Definition of the abstract <see cref="AEntity"/> base class.
    /// </summary>
    public abstract class AEntity : INotifyPropertyChanged
    {
        #region Fields

        /// <summary>
        /// Stores the entity name.
        /// </summary>
        private string mName;

        /// <summary>
        /// Stores the entity state.
        /// </summary>
        private EntityState mState;

        /// <summary>
        /// Stores the entity position.
        /// </summary>
        private Location mPosition;

        /// <summary>
        /// Stores the entity orientation.
        /// </summary>
        private Direction mOrientation;

        /// <summary>
        /// Stores the entity team.
        /// </summary>
        private Team mTeam;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired on property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets or sets the entity name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
            set
            {
                this.mName = value;
                this.NotifyPropertyChanged( "Name" );
            }
        }

        /// <summary>
        /// Gets or sets the entity state.
        /// </summary>
        public EntityState State
        {
            get
            {
                return this.mState;
            }
            set
            {
                this.mState = value;
                this.NotifyPropertyChanged( "State" );
            }
        }

        /// <summary>
        /// Gets or sets the entity position.
        /// </summary>
        public Location Position
        {
            get
            {
                return this.mPosition;
            }
            set
            {
                this.mPosition = value;
                this.NotifyPropertyChanged( "Position" );
            }
        }

        /// <summary>
        /// Gets or sets the entity orientation.
        /// </summary>
        public Direction Orientation
        {
            get
            {
                return this.mOrientation;
            }
            set
            {
                this.mOrientation = value;
                this.NotifyPropertyChanged( "Orientation" );
            }
        }

        /// <summary>
        /// Gets the entity team.
        /// </summary>
        public Team Team
        {
            get
            {
                return this.mTeam;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AEntity"/> class.
        /// </summary>
        /// <param name="pName">The entity unique name.</param>
        /// <param name="pTeam">The entity team.</param>
        protected AEntity(string pName, Team pTeam)
        {
            this.mName = pName;
            this.mTeam = pTeam;
            if ( this.mTeam == null )
            {
                this.mTeam = new Team();
            }

            this.mState = EntityState.Alive;
            this.mPosition = new Location();
            this.mOrientation = new Direction();
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Notifies a property changed
        /// </summary>
        /// <param name="pPropertyName"></param>
        protected void NotifyPropertyChanged(string pPropertyName)
        {
            if ( this.PropertyChanged != null )
            {
                this.PropertyChanged( this, new PropertyChangedEventArgs( pPropertyName ) );
            }
        }

        #endregion Methods
    }
}
