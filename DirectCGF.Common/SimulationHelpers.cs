﻿using Google.Protobuf;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace DirectCGF.Common
{
    /// <summary>
    /// Definition of the <see cref="SimulationHelpers"/> class.
    /// </summary>
    public static class SimulationHelpers
    {
        #region Methods

        /// <summary>
        /// Creates a message from the given entity.
        /// </summary>
        /// <param name="pEntity">The entity</param>
        /// <param name="pIsCurrent">The flag indicating whether the entity is current or not.</param>
        /// <returns>The message.</returns>
        public static IMessage CreateFromEntity(AEntity pEntity, bool pIsCurrent)
        {
            if ( pEntity == null )
            {
                return null;
            }

            DCGF.ProtoBuf.Entity lMessage    = new DCGF.ProtoBuf.Entity();
            lMessage.MessageType = new DCGF.ProtoBuf.GenericType() { TypeName = string.Format( "[{0};{1}]", typeof(DCGF.ProtoBuf.Entity).FullName, "DCGF.ProtoBuf" ) };
            lMessage.Name      = pEntity.Name;
            lMessage.IsCurrent = pIsCurrent;

            DCGF.ProtoBuf.Location lPosition = new DCGF.ProtoBuf.Location();
            lPosition.Latitude  = pEntity.Position.Latitude;
            lPosition.Longitude = pEntity.Position.Longitude;
            lPosition.Altitude  = pEntity.Position.Altitude;
            lMessage.Position   = lPosition;

            DCGF.ProtoBuf.Direction lDirection = new DCGF.ProtoBuf.Direction();
            lDirection.Yaw   = pEntity.Orientation.Yaw;
            lDirection.Pitch = pEntity.Orientation.Pitch;
            lDirection.Roll  = pEntity.Orientation.Roll;
            lMessage.Orientation = lDirection;

            lMessage.State = (DCGF.ProtoBuf.Entity.Types.EntityState)(int)pEntity.State;

            DCGF.ProtoBuf.Team lTeam = new DCGF.ProtoBuf.Team();
            lTeam.ColorId = pEntity.Team.ColorId;
            lTeam.Name    = pEntity.Team.Name;
            lMessage.Side = lTeam;

            return lMessage;
        }

        /// <summary>
        /// Create an entity removal message
        /// </summary>
        /// <param name="pEntityName"></param>
        /// <returns></returns>
        public static IMessage RemoveEntity(string pEntityName)
        {
            DCGF.ProtoBuf.Remove lMessage = new DCGF.ProtoBuf.Remove();
            lMessage.MessageType = new DCGF.ProtoBuf.GenericType() { TypeName = string.Format( "[{0};{1}]", typeof(DCGF.ProtoBuf.Remove).FullName, "DCGF.ProtoBuf" ) };
            lMessage.Name = pEntityName;
            return lMessage;
        }

        /// <summary>
        /// Creates or updates an entity from a message.
        /// </summary>
        /// <param name="pEntityType">The entity type to create.</param>
        /// <param name="pEntities">The entity set to look for the entity to update.</param>
        /// <param name="pMessage"></param>
        /// <param name="pIsNew">The flag indicating whether it is a new entity or not.</param>
        /// <param name="pMustRemove">The flag indicating whether the message indicates to remove an entity or not.</param>
        /// <param name="pIsCurrent">The flag indicating whether the entity is current or not.</param>
        /// <returns></returns>
        public static AEntity  CreateFromMessage(Type pEntityType, ref Dictionary<string, AEntity> pEntities, IMessage pMessage, out bool pIsNew, out bool pMustRemove, out bool pIsCurrent)
        {
            AEntity lEntity = null;

            pIsNew = false;
            pIsCurrent = false;
            pMustRemove = pMessage is DCGF.ProtoBuf.Remove;
            if ( pMustRemove )
            {
                DCGF.ProtoBuf.Remove lRemoveSms = pMessage as DCGF.ProtoBuf.Remove;
                pEntities.TryGetValue( lRemoveSms.Name, out lEntity );
                pEntities.Remove( lRemoveSms.Name );
            }

            DCGF.ProtoBuf.Entity lMessage = pMessage as DCGF.ProtoBuf.Entity;
            if ( lMessage == null )
            {
                return lEntity;
            }
            
            if ( pEntities.TryGetValue( lMessage.Name, out lEntity ) == false )
            {
                // Create.
                ConstructorInfo lCtor = pEntityType.GetConstructor( new Type[] { typeof(string), typeof(Team) } ) as ConstructorInfo;
                if ( lCtor != null )
                {
                    Team lTeam = new Team( lMessage.Side.Name, lMessage.Side.ColorId );
                    lEntity = lCtor.Invoke( new object[] { lMessage.Name, lTeam } ) as AEntity;
                    pEntities.Add( lEntity.Name, lEntity );
                    pIsNew = true;
                }
            }
            
            // Update
            if ( lEntity != null )
            {
                if ( lMessage.Position != null )
                {
                    lEntity.Position = new Location( lMessage.Position.Latitude, lMessage.Position.Longitude, lMessage.Position.Altitude );
                }
                if ( lMessage.Orientation != null )
                {
                    lEntity.Orientation = new Direction( lMessage.Orientation.Yaw, lMessage.Orientation.Pitch, lMessage.Orientation.Roll );
                }
                lEntity.State = (EntityState)(int)lMessage.State;

                pIsCurrent = lMessage.IsCurrent;
            }

            return lEntity;
        }

        #endregion Methods
    }
}
