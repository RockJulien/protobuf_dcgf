﻿using Castle.Zmq;
using System;

namespace DirectCGF.Common
{
    /// <summary>
    /// Definition of the DCGF ZMQ <see cref="Context"/> class.
    /// </summary>
    public class Context : IDisposable
    {
        #region Fields

        /// <summary>
        /// Stores the Zmq context
        /// </summary>
        private IZmqContext mContext;

        #endregion Fields

        #region Constructor

        /// <summary>
        /// Initializes the <see cref="Context"/> class.
        /// </summary>
        /// <param name="pIOThreadCount">The I/O thread count.</param>
        public Context(int pIOThreadCount = 1)
        {
            this.mContext = new Castle.Zmq.Context( pIOThreadCount, 10 );
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Implicit cast from DCGF context into a ZMQ context.
        /// </summary>
        /// <param name="pContext"></param>
        public static implicit operator Castle.Zmq.Context(Context pContext)
        {
            return pContext.mContext as Castle.Zmq.Context;
        }

        /// <summary>
        /// Releases resources.
        /// </summary>
        public void Dispose()
        {
            if ( this.mContext != null )
            {
                this.mContext.Dispose();
                this.mContext = null;
            }
        }

        #endregion Methods
    }
}
