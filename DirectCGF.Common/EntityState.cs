﻿namespace DirectCGF.Common
{
    /// <summary>
    /// Definition of the <see cref="EntityState"/> enumeration.
    /// </summary>
    public enum EntityState : int
    {
        /// <summary>
        /// Alive
        /// </summary>
        Alive = 0,

        /// <summary>
        /// Freezed
        /// </summary>
        Freezed = 1,

        /// <summary>
        /// Dead
        /// </summary>
        Dead = 2
    }
}
