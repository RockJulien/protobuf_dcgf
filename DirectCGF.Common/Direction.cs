﻿namespace DirectCGF.Common
{
    /// <summary>
    /// Definition of the <see cref="Direction"/> class.
    /// </summary>
    public class Direction
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Yaw component.
        /// </summary>
        public double Yaw
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Pitch component.
        /// </summary>
        public double Pitch
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Roll component.
        /// </summary>
        public double Roll
        {
            get;
            set;
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Direction"/> class.
        /// </summary>
        public Direction() :
        this( 0.0, 0.0, 0.0 )
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Direction"/> class.
        /// </summary>
        /// <param name="pYaw"></param>
        /// <param name="pPitch"></param>
        /// <param name="pRoll"></param>
        public Direction(double pYaw, double pPitch, double pRoll)
        {
            this.Yaw   = pYaw;
            this.Pitch = pPitch;
            this.Roll  = pRoll;
        }

        #endregion Constructor
    }
}
