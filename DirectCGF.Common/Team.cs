﻿using DirectCGF.Constants;

namespace DirectCGF.Common
{
    /// <summary>
    /// Definition of the <see cref="Team"/> class.
    /// </summary>
    public class Team
    {
        #region Fields

        /// <summary>
        /// Stores the team name.
        /// </summary>
        private string mName;

        /// <summary>
        /// Stores the team color Id.
        /// </summary>
        private int mColorId;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets or sets the team name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.mName;
            }
            set
            {
                this.mName = value;
            }
        }

        /// <summary>
        /// Gets or sets the team color Id.
        /// </summary>
        public int ColorId
        {
            get
            {
                return this.mColorId;
            }
            set
            {
                this.mColorId = value;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Team"/> class.
        /// </summary>
        public Team() :
        this( "Default", Constants.Constants.WHITETEAMCOLORID )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Team"/> class.
        /// </summary>
        /// <param name="pName">The team name</param>
        /// <param name="pColorId">The team color Id</param>
        public Team(string pName, int pColorId)
        {
            this.mName = pName;
            this.mColorId = pColorId;
        }

        #endregion Constructor
    }
}
